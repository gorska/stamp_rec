from PIL import ImageFilter, Image


class Stamp():

    def __init__(self, path):
        self.path = path
        self.img = Image.open(path)

    def image_preprocessing(self):
        img = self.img.filter(ImageFilter.SHARPEN)
        img = img.convert('L')
        img = img.point(lambda y: 0 if y < 120 else 255)
        img = img.filter(ImageFilter.EDGE_ENHANCE_MORE)
        img = img.filter(ImageFilter.SMOOTH)
        return img
